package com.epam.engx.story;

import com.epam.engx.story.domain.User;
import com.epam.engx.story.repository.InMemoryUserRepository;
import com.epam.engx.story.repository.UnitOfWork;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UnitOfWorkPatternStoryJavaApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(UnitOfWorkPatternStoryJavaApplication.class, args);
    }

    @Override
    public void run(String... args) {
        var unitOfWork = new UnitOfWork();
        var repo = new InMemoryUserRepository(unitOfWork);
        var user = User.builder().id(1).name("John Doe").build();

        repo.add(user);
        user.setName("John Doe Jr.");
        repo.update(user);
        repo.delete(user);

        unitOfWork.commit();
    }
}
