package com.epam.engx.story.repository;

import com.epam.engx.story.domain.User;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class InMemoryUserRepository implements UserRepository {
    private final UnitOfWork unitOfWork;

    @Override
    public void add(User user) {
        unitOfWork.registerNew(user);
    }

    @Override
    public void update(User user) {
        unitOfWork.registerUpdated(user);
    }

    @Override
    public void delete(User user) {
        unitOfWork.registerDeleted(user);
    }
}
