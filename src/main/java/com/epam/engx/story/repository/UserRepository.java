package com.epam.engx.story.repository;

import com.epam.engx.story.domain.User;

public interface UserRepository {
    void add(User user);
    void update(User user);
    void delete(User user);

}
