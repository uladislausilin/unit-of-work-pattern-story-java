package com.epam.engx.story.repository;

import java.util.ArrayList;
import java.util.List;

import com.epam.engx.story.domain.User;

public class UnitOfWork {
    private final List<User> newUser = new ArrayList<>();
    private final List<User> updatedUser = new ArrayList<>();
    private final List<User> deletedUser = new ArrayList<>();

    public void commit() {
        newUser.forEach(user -> System.out.println("Adding new user: " + user.getName()));
        updatedUser.forEach(user -> System.out.println("Updating user: " + user.getName()));
        deletedUser.forEach(user -> System.out.println("Deleting user: " + user.getName()));

        // After logging, clear the lists to simulate committing the transaction
        newUser.clear();
        updatedUser.clear();
        deletedUser.clear();
    }

    public void registerNew(User user) {
        newUser.add(user);
    }

    public void registerUpdated(User user) {
        updatedUser.add(user);
    }

    public void registerDeleted(User user) {
        deletedUser.add(user);
    }
}
